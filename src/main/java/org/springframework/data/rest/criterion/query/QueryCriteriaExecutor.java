/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.data.rest.criterion.query;


import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public interface QueryCriteriaExecutor {

    default QueryCriteria getExecutableQueryCriteria(QueryCriteria queryCriteria) {
        executeQueryCriteria(queryCriteria);
        return queryCriteria;
    }

    default void executeQueryCriteria(QueryCriteria queryCriteria) {
        executeJunctionQueryCriteria(queryCriteria);
        executeInclusionQueryCriteria(queryCriteria);
    }

    default void executeJunctionQueryCriteria(QueryCriteria queryCriteria) {
        if(QueryCriteriaUtil.isJunctionCriterion(queryCriteria)) {
            queryCriteria.getJunctionQueries().forEach(junction -> executeQueryCriteria(junction));
        }
    }

    default void executeInclusionQueryCriteria(QueryCriteria queryCriteria) {
        if(QueryCriteriaUtil.isInclusionCriterion(queryCriteria)) {
            if(queryCriteria.getValues() == null) {
                queryCriteria.setValues(new ArrayList());
            }
            queryCriteria.getValues().
                    addAll(getQueryCriteriaValues(getExecutableQueryCriteria(queryCriteria.getInclusionQuery())));
        }
    }

    List getQueryCriteriaValues(QueryCriteria queryCriteria);

}
