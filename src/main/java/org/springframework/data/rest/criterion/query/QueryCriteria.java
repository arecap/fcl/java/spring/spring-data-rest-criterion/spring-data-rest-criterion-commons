/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.data.rest.criterion.query;


import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public class QueryCriteria implements Serializable {


    private String entityName;

    private String[] associatedPaths;

    private String propertyName;

    private CriterionDefinition criterion;

    private Set<ProjectionQuery> projections;

    private List values;

    private QueryCriteria inclusionQuery;

    private Set<QueryCriteria> junctionQueries;

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String[] getAssociatedPaths() {
        return associatedPaths;
    }

    public void setAssociatedPaths(String[] associatedPaths) {
        this.associatedPaths = associatedPaths;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public CriterionDefinition getCriterion() {
        return criterion;
    }

    public void setCriterion(CriterionDefinition criterion) {
        this.criterion = criterion;
    }

    public Set<ProjectionQuery> getProjections() {
        return projections;
    }

    public void setProjections(Set<ProjectionQuery> projections) {
        this.projections = projections;
    }

    public QueryCriteria getInclusionQuery() {
        return inclusionQuery;
    }

    public void setInclusionQuery(QueryCriteria inclusionQuery) {
        this.inclusionQuery = inclusionQuery;
    }

    public Set<QueryCriteria> getJunctionQueries() {
        return junctionQueries;
    }

    public void setJunctionQueries(Set<QueryCriteria> junctionQueries) {
        this.junctionQueries = junctionQueries;
    }

    public List getValues() {
        return values;
    }

    public void setValues(List values) {
        this.values = values;
    }

}
